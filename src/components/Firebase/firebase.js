import app from 'firebase/app';
import 'firebase/auth';
import 'firebase/database';


const config = {
    apiKey: "AIzaSyAKBMof2l-XGuOgsISpaFgZH75mL2qs7PQ",
    authDomain: "authentication-app-73f37.firebaseapp.com",
    databaseURL: "https://authentication-app-73f37.firebaseio.com",
    projectId: "authentication-app-73f37",
    storageBucket: "authentication-app-73f37.appspot.com",
    messagingSenderId: "960729841",
    appId: "1:960729841:web:ee09e5c9f13283ea3b0f50"
};

class Firebase {
    constructor() {
        app.initializeApp(config);

        this.auth = app.auth();
        this.db = app.database();
    }
    // *** Auth API ***

    doCreateUserWithEmailAndPassword = (email, password) =>
        this.auth.createUserWithEmailAndPassword(email, password);

    doSignInWithEmailAndPassword = (email, password) =>
        this.auth.signInWithEmailAndPassword(email, password);

    doSignOut = () => this.auth.signOut();

    doPasswordReset = email => this.auth.sendPasswordResetEmail(email);

    doPasswordUpdate = password =>
        this.auth.currentUser.updatePassword(password);

    // *** User API ***

    user = uid => this.db.ref(`users/${uid}`);

    users = () => this.db.ref('users');
}

export default Firebase;