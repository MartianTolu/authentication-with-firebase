# Authentication-With- Firebase

A simple project shows all the steps of Authentication using firebase.

# To clone the project

`$ git clone https://gitlab.com/MartianTolu/authentication-with-firebase.git`

After cloning, cd into directory/folder

`cd authentication-with-firebase`

# Setup
You will need to install some packages, if they are not yet installed on your machine:

- Node.js (v10.0.0 or higher; LTS)

# Installation

`yarn`

# Running The App

`yarn start`
